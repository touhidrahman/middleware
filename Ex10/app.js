/**
 * Task 10.1.1
 */

let no = 3;

(function(x) {
    if (x % 2 == 0) {
        console.log("1)\t" + no + " is Even Number");
    } else {
        console.log("1)\t" + no + " is Odd Number");
    }
})(no);





/**
 * Task 10.1.2
 */
function sumThird() {
    let sum = 0;
    for (let i = 2; i <= 100; i += 3) {
        sum += i;
    }
    return sum;
}

function sumThirdWhile() {
    let sum = 0;
    let i = 2;
    while (i <= 100) {
        sum += i;
        i += 3;
    }
    return sum;
}
console.log("2)\t Sum of every third number between 0-100:");
console.log("\t1. For Loop Version: " + sumThird());
console.log("\t2. While Loop Version: " + sumThirdWhile());




/**
 * Task 10.1.3
 */
let data = "Hello Winter Semester 2016";
const fs = require('fs');
console.log("3)\tCreating Text File at dump.txt");
fs.writeFile('dump.txt', data, function(err) {
    if (err) {
        console.log(err);
    }
});





/**
 * Task 10.1.4
 */
let cel = 42;

let c2f = temp => { return temp * 9 / 5 + 32; }

console.log("4)\t" + cel + " C to F: " + c2f(cel));
