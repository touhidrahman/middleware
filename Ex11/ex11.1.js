var express = require('express')
var app = express()
 
app.get('/:name', function (req, res) {
  res.send('Hello ' + req.params.name)
})
 
app.listen(3000, function() {
  console.log('Express server listening on port 3000');
})