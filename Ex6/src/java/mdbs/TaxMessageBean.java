/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdbs;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 *
 * @author touhid
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/myQueue")
    ,
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class TaxMessageBean implements MessageListener {
    
    public TaxMessageBean() {
    }
    
    @Override
    public void onMessage(Message message) {
        TextMessage msg = (TextMessage) message;
        
//        File file = new File("results.csv");
//        try {
//            file.createNewFile();
//        } catch (IOException ex) {
//            Logger.getLogger(TaxMessageBean.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
        try {
            FileWriter fw = new FileWriter("/home/touhid/Codes/Java/Middleware/Ex6/results.csv", true);
            fw.append(msg.getText());
            fw.append("\n");
            fw.flush();
            fw.close();
        } catch (IOException | JMSException ex) {
            Logger.getLogger(TaxMessageBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
