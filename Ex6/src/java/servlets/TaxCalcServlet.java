/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author touhid
 */
public class TaxCalcServlet extends HttpServlet {

    @Resource(mappedName="jms/myConnectionFactory")
    private ConnectionFactory cf;
    
    @Resource(mappedName="jms/myQueue")
    private Queue q;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws javax.jms.JMSException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            int amount = Integer.parseInt(request.getParameter("amount"));
            int tax = Integer.parseInt(request.getParameter("tax"));
            int result = amount + amount * tax / 100;
            String currency = request.getParameter("currency");
            
            Connection con = cf.createConnection();
            // create session
            Session s = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
            // create producer
            MessageProducer mp = s.createProducer(q);
            // create msg
            TextMessage msg = s.createTextMessage();
            msg.setText("Amount: " + amount + " Tax: " + tax + " Result: " + result);
            // send message
            mp.send(msg);
            
            // close
            mp.close();
            s.close();
            con.close();
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet TaxCalcServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Message sent to queue. <a href='index.html'>Go Back</a></h1>");
            out.println("</body>");
            out.println("</html>");
        } catch (JMSException ex) {
            Logger.getLogger(TaxCalcServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
