package ex7;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
/**
 *
 * @author touhid
 */
public class Ex7 {
    
    public static void main(String[] args) throws Exception {
        
        QueueConnection queueCon = null;
    
        try {
            Context ctx = new InitialContext();

            QueueConnectionFactory qcf = (QueueConnectionFactory) ctx.lookup( "jms/myConnectionFactory" );
            queueCon = qcf.createQueueConnection();

            QueueSession queueSession = queueCon.createQueueSession( false, Session.AUTO_ACKNOWLEDGE );

            Queue queue = (Queue) ctx.lookup("jms/myQueue");
            QueueReceiver receiver = queueSession.createReceiver( queue );

            queueCon.start();
            System.out.println("Listening for message!" );
            Message m = receiver.receive();
            if( m != null && m instanceof TextMessage ) {
                TextMessage message = (TextMessage) m;
                System.out.println( "Received message = " + message.getText());
            }
        } catch (NamingException | JMSException ex) {
            Logger.getLogger(Ex7.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
              queueCon.close();
        }
    }
    
}
