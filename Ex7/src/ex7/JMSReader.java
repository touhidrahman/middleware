package ex7;

import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.*;
import javax.jms.Queue;

/**
 *
 * @author touhid
 */
public class JMSReader {
    @Resource(mappedName="jms/myConnectionFactory")
    private  static ConnectionFactory connectionFactory;
    
    @Resource(mappedName="jms/myQueue")
    private static Queue queue;
    
    public static void main(String[] args) {
      JMSContext jmsContext = connectionFactory.createContext();
     
     JMSConsumer jmsConsumer =jmsContext.createConsumer(queue);
    
     System.out.println("Receiving message....");
     
     String message=jmsConsumer.receiveBody(String.class);
     
     System.out.println("Message received: "+message);
    }
}
