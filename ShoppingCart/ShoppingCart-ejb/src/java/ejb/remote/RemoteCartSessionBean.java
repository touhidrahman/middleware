/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb.remote;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateful;

/**
 *
 * @author touhid
 */
@Stateful
public class RemoteCartSessionBean implements RemoteCartInterface {

    private static List<String> cartItems = new ArrayList<String>();
    
    @Override
    public void add(String item) {
        cartItems.add(item);
    }

    @Override
    public void remove(String item) {
        cartItems.remove(item);
    }

    @Override
    public int getSize() {
        return cartItems.size();
    }

    @Override
    public List<String> getCart() {
        return cartItems;
    }

    
}
