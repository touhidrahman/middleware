/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shoppingcartclient;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import ejb.remote.RemoteCartInterface;

/**
 *
 * @author touhid
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            InitialContext ic = new InitialContext();
            // using glassfish xml descriptor for JNDI
            RemoteCartInterface cart = (RemoteCartInterface) ic.lookup("RemoteCart"); 

            cart.add("Banana");
            cart.add("Orange");
            cart.add("Apple");
            
            System.out.println("You have " + cart.getSize() + " items in your cart.");
            
        } catch (NamingException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
