/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb.remote;

import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author touhid
 */
@Remote
public interface RemoteCartInterface {

    void add(String item);
    
    void remove(String item);
    
    int getSize();
    
    List<String> getCart();
    
}
