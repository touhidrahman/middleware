package task2;

import beans.TaxSessionBeanRemote;
import javax.ejb.Stateless;

/**
 *
 * @author touhid
 */
@Stateless
public class TaxCalculatorBean implements TaxCalculatorBeanLocal, TaxSessionBeanRemote {
    @Override
    public int calcTax(int amount, int tax) {
        return (amount + amount * tax / 100);
    }
}
