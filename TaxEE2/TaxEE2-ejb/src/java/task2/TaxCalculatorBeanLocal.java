/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task2;

import javax.ejb.Local;

/**
 *
 * @author touhid
 */
@Local
public interface TaxCalculatorBeanLocal {
    int calcTax(int amount, int tax);
}
