package app;

import java.io.IOException;
import java.io.PrintWriter;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.jms.Queue;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import task2.TaxCalculatorBeanLocal;

/**
 *
 * @author touhid
 */
public class TaxServlet extends HttpServlet {
    
    @EJB
    TaxCalculatorBeanLocal taxCalculatorBean;
    
    @Resource(mappedName = "jms/MiddlewareExercisesFactory")
    private static ConnectionFactory cf;
    
    @Resource(mappedName = "MiddlewareQueue")
    private static Queue q;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String amount = request.getParameter("amount");
        String tax = request.getParameter("tax");
        String currency = request.getParameter("currency");
        
        JMSContext jmsCtx = cf.createContext();
        JMSProducer jmsProducer = jmsCtx.createProducer();
        
        jmsProducer.send(q, amount);
        jmsProducer.send(q, tax);
        jmsProducer.send(q, currency);
    
    

        
//        int res = taxCalculatorBean.calcTax(amount, tax);
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>TaxServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>The data is written in message queue!</h1>");
//            out.println("<h1>The Amount of Tax is " + res + " " + currency + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
