package ex8.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author touhid
 */
@WebServlet(name = "DisplayServlet", urlPatterns = {"/DisplayServlet"})
public class DisplayServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MainServlet</title>");            
            out.println("</head>");
            out.println("<body>");

            
            
            String dbUrl = "jdbc:mysql://localhost:3306/test";
            String user = "root";
            String pass = "root";

            try (Connection conn = DriverManager.getConnection(dbUrl, user, pass))
            {
                if (conn != null) {
                    System.out.println("Connected Succesfully");
                }

                String date = request.getParameter("date");
                
                String sql = "SELECT * FROM taxes WHERE created=?";
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setDate(1, java.sql.Date.valueOf(date));
                ResultSet rs = stmt.executeQuery();

                if (!rs.first()) {
                    out.println("<h1>No Result!</h1>");
                } else {
                    out.println("<table><tr><th>Amount</th><th>Tax</th><th>Result</th></tr>");
                    while(rs.next()){
                        String curr = rs.getString("currency");
                        int amount = rs.getInt("amount");
                        int result = rs.getInt("result");
                        int tax = rs.getInt("tax");
                        out.println("<tr><td>" + amount + "</td><td>" + tax + "</td><td>" + result + "</td></tr>");
                    }
                    out.println("</table>");
                }

                              

            } catch (SQLException ex) {
                ex.printStackTrace();
            }

            
            out.println("<p><a href='index.html'>Input Record</a></p>");
            out.println("</body>");
            out.println("</html>");
            

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
