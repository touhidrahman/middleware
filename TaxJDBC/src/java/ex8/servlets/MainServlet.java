package ex8.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;

/**
 *
 * @author touhid
 */
@WebServlet(name = "MainServlet", urlPatterns = {"/MainServlet"})
public class MainServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        

        
                
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MainServlet</title>");            
            out.println("</head>");
            out.println("<body>");

            
            
            String dbUrl = "jdbc:mysql://localhost:3306/test";
            String user = "root";
            String pass = "root";

            try (Connection conn = DriverManager.getConnection(dbUrl, user, pass))
            {
                if (conn != null) {
                    System.out.println("Connected Succesfully");
                }

                int amount = Integer.parseInt(request.getParameter("amount"));
                int tax = Integer.parseInt(request.getParameter("tax"));
                int result = amount + amount * tax /100;

                String sql = "INSERT INTO taxes (amount, tax, currency, result, created) VALUES (?, ?, ?, ?, ?)";
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1, amount);
                stmt.setInt(2, tax);
                stmt.setString(3, request.getParameter("currency"));
                stmt.setInt(4, result);
                stmt.setDate(5, java.sql.Date.valueOf(java.time.LocalDate.now()));

                if(stmt.executeUpdate() > 0){
                    out.println("<h1>Record saved succefully</h1>");
                } else {
                    out.println("<h1>Record couldn't be saved</h1>");
                }
                                

            } catch (SQLException ex) {
                ex.printStackTrace();
            }

            
            out.println("<p><a href='display.html'>Display Records</a></p>");
            out.println("</body>");
            out.println("</html>");
            

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
