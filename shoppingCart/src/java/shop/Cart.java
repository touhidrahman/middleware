package shop;

import java.util.HashMap;

/**
 *
 * @author touhid
 */
public class Cart {
    
    HashMap<String, Integer> cartItems;
    
    public Cart(){
        cartItems = new HashMap<>();
    }
    
    public HashMap getCartItems(){
        return cartItems;
    }
    
    public void addToCart(String item, int price){
        cartItems.put(item, price);
    }
    
    public void deleteFromCart(String itemId){
        cartItems.remove(itemId);
    }
}
